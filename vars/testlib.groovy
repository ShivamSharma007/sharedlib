def userInfo(){
    echo "going to shared lib"
    def USER_ID=""
    def USER_EMAIL=""
    try {
        wrap([$class: 'BuildUser']) {
            USER_ID=BUILD_USER_ID.toLowerCase()
            USER_EMAIL=BUILD_USER_EMAIL.toLowerCase()
        }
    } catch (e) {
        USER_ID='trigger'
        USER_EMAIL=''
    }
    echo "${BUILD_USER_EMAIL}"
    return BUILD_USER_EMAIL
}
